#!/usr/bin/env groovy

def call() {
	dir("app"){
		echo "Updating the application version..."
		sh " npm version patch "
		def packageJson = readJSON file: 'package.json'
		def version = packageJson.version
		env.IMAGE_NAME = "$version-$BUILD_NUMBER"
	}
}