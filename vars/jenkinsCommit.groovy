#!/usr/bin/env groovy

def call() {
	withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
		    sh 'git config --global user.email "jenkins@example.com"' 
			sh 'git config --global user.name "jenkins"'

			sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/omacodes98/nodejs-application-ci-pipeline.git"
			sh 'git add .'
			sh 'git commit -m "ci: version bump"'
			sh 'git push origin HEAD:Jenkins-shared-library'
	}
}