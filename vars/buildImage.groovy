#!/usr/bin/env groovy

def call() {
	echo "building the image"
		withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]){
		    sh "docker build -t omacodes98/nodejs-app:${IMAGE_NAME} ."
			sh "echo ${PASS} | docker login -u ${USER} --password-stdin"
			sh "docker push omacodes98/nodejs-app:${IMAGE_NAME}"
		}
}